public class FindPrimes {
    public static void main(String[] args){
        int max = Integer.parseInt(args[0]);

        //Print the numbers less than max
        //For each number less than max
        for (int number = 2; number < max; number ++) {
            // Let divisor = 2;
            int divisor = 2;
            // Let isPrime
            boolean isPrime = true;
            // while divisor less than number
            while(divisor < number  && isPrime) {
                //if the number is divisible by divisor
                if (number % divisor == 0)
                    isPrime = false; // isPrime = false
                    // increment divisor
                divisor++;

            }
            //If the number is prime
            if (isPrime)
                System.out.print(number + " "); //print the number
        }
    }
}
